truncate users cascade;
truncate authorities cascade;

insert into users values('john', '{bcrypt}$2a$10$2OPCwbH78n9AdfuD5Nq8ROT4atyfguBGaoOAYGRZ2btdlj9lzu1DS', true);
insert into authorities values('john', 'ADMIN');

INSERT INTO users VALUES ('paul','{MD5}6c63212ab48e8401eaf6b59b95d816a9',TRUE);
INSERT INTO users VALUES ('pierre','{noop}pierre',TRUE);

INSERT INTO authorities VALUES ('paul', 'USER');
INSERT INTO authorities VALUES ('pierre', 'USER');
