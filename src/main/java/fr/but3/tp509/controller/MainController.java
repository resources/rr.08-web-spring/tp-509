package fr.but3.tp509.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @GetMapping
    public String home() {
        return "index";
    }

    @GetMapping(value = "/public")
    public String v1() {
        return "public/v1";
    }

    @GetMapping(value = "/private")
    public String v2() {
        return "private/v2";
    }
}
