package fr.but3.tp509;

import jakarta.servlet.DispatcherType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class Security {

    @Autowired
    DataSource dataSource;

    @Bean
    public JdbcUserDetailsManager udm() {
        return new JdbcUserDetailsManager(dataSource);
    }

    @Bean
    public SecurityFilterChain mesautorisations(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
        MvcRequestMatcher.Builder mvc = new MvcRequestMatcher.Builder(introspector);
        return http.authorizeHttpRequests((authorize) -> authorize.dispatcherTypeMatchers(DispatcherType.FORWARD)
                                                                  .permitAll().requestMatchers(mvc.pattern("/"))
                                                                  .permitAll().requestMatchers(mvc.pattern("/private"))
                                                                  .authenticated().anyRequest().permitAll())
                   .formLogin(Customizer.withDefaults()).logout((configurer) -> {
                    configurer.logoutUrl("/logout").logoutSuccessUrl("/public");
                }).rememberMe((configurer) -> configurer.useSecureCookie(true)).build();
    }

//    @Bean
//    public UserDetailsService mesutilisateurs() {
//        String usersByUsernameQuery = "select username, password, enabled from t1 where username = ?";
//        String authsByUserQuery = "select username, authority from t2 where username = ?";
//
//        JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
//
//        userDetailsManager.setUsersByUsernameQuery(usersByUsernameQuery);
//        userDetailsManager.setAuthoritiesByUsernameQuery(authsByUserQuery);
//
//        return users;
//    }

//    @Bean
//    public PasswordEncoder encoder() {
//        return new BCryptPasswordEncoder();
//    }
//
}
// $2a$10$2OPCwbH78n9AdfuD5Nq8ROT4atyfguBGaoOAYGRZ2btdlj9lzu1DS
